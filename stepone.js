"use strict";


let tab = function () {
	let tabNav = document.querySelectorAll('.tabs-list'),
		tabContent = document.querySelectorAll('.content'),
		tabName;

	tabNav.forEach(item => {
		item.addEventListener('click', selectTabNav)
	});
	function selectTabNav() {
		tabNav.forEach(item => {
			item.classList.remove('active');
		});
		this.classList.add('active');
		tabName = this.getAttribute('data-tab-name');
		selectTabContent(tabName);
	}
	function selectTabContent(tabName) {
		tabContent.forEach(item => {
			item.classList.contains(tabName) ? item.classList.add('active') : item.classList.remove('active');
		})
	}

};

tab();
// ----------------------------------------------------------------
let tabs = document.querySelectorAll(".another-image");
function showImage() {
	tabs.forEach(elem => {
		elem.style.display = "block";
	})
	document.getElementById("btn").style.display = "none";
};

let filter = document.querySelectorAll(".image-item");
document.getElementById("tabFilter").addEventListener("click", event => {
	if (event.target.tagName !== "LI") return false;
	let liClass = event.target.dataset["f"];
	filter.forEach(elem => {
		elem.classList.remove("hide");
		if (!(elem.classList.contains(liClass) || liClass === "all")) {
			elem.classList.add("hide");
		}

	});

});
// ------------------------------
let prev = document.getElementById("btn-prev");
let next = document.getElementById("btn-next");
let slide = document.querySelectorAll(".slide");
let unit = document.querySelectorAll(".unit");

let index = 0;
// -----
let activeSlide = x =>{
for(let slides of slide){
	slides.classList.remove("open");
}
   slide[x].classList.add("open");
}
// ----
let activeUnit = x =>{
	for(let units of unit){
		units.classList.remove("open");
	}
		unit[x].classList.add("open");
	}
// ------
let currentSlide = index =>{
	activeSlide(index);
	activeUnit(index);
}
// -------
let nextSlide = () =>{
	if(index === slide.length-1){
		index = 0;
		currentSlide(index);
	}else{
		index++;
		currentSlide(index);
	}
}
// ------
let prevSlide = () =>{
	if(index === 0){
		index = slide.length-1;
		currentSlide(index);
	}else{
		index--;
		currentSlide(index);
	}
}
// ------
unit.forEach((item,indexUnit) => {
item.addEventListener("click", ()=>{
	index = indexUnit;
	currentSlide(index);
})
})
// -------
next.addEventListener("click", nextSlide);
prev.addEventListener("click", prevSlide);